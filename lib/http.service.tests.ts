import { Subject, Observable, Observer } from 'rxjs';
import { RequestOptions } from '@angular/http';

import { HttpUtility } from './http.service';

interface IMockHttp {
	get: Sinon.SinonSpy;
	post: Sinon.SinonSpy;
	put: Sinon.SinonSpy;
	delete: Sinon.SinonSpy;
}

interface IMockInterceptor {
	handleSuccess: Sinon.SinonSpy;
	handleError: Sinon.SinonSpy;
	handleComplete:Sinon.SinonSpy;
}

describe('HttpUtility', (): void => {
	let http: HttpUtility;
	let mockHttpImplementation: IMockHttp;
	let interceptor: IMockInterceptor;

	beforeEach((): void => {

		const mockRequest = new Observable((observer: Observer<any>) => {
			observer.next({});
			observer.complete();
		});

		mockHttpImplementation = {
			get: sinon.spy(() => mockRequest),
			post: sinon.spy(() => mockRequest),
			put: sinon.spy(() => mockRequest),
			delete: sinon.spy(() => mockRequest),
		};

		interceptor = {
			handleSuccess: sinon.spy(value => value),
			handleError: sinon.spy(error => error),
			handleComplete: sinon.spy()
		};

		http = new HttpUtility(<any>mockHttpImplementation, interceptor);
	});

	it('should call the interceptor handle complete event after a get subscribe', (done) => {

		http.get('/test').subscribe(() => {
			sinon.assert.notCalled(interceptor.handleComplete);
		}, null, () => {
			sinon.assert.calledOnce(interceptor.handleComplete);
			done();
		});

	});

	it('should call the interceptor handle complete event after a put subscribe', (done) => {

		http.put('/test', {}).subscribe(() => {
			sinon.assert.notCalled(interceptor.handleComplete);
		}, null, () => {
			sinon.assert.calledOnce(interceptor.handleComplete);
			done();
		});
	});

	it('should call the interceptor handle complete event after a post subscribe', (done) => {

		http.post('/test', {}).subscribe(() => {
			sinon.assert.notCalled(interceptor.handleComplete);
		}, null, () => {
			sinon.assert.calledOnce(interceptor.handleComplete);
			done();
		});
	});

	it('should call the interceptor handle complete event after a delete subscribe', (done) => {

		http.delete('/test', {}).subscribe(() => {
			sinon.assert.notCalled(interceptor.handleComplete);
		}, null, () => {
			sinon.assert.calledOnce(interceptor.handleComplete);
			done();
		});
	});

	it('should make a get request with any specified query string parameters', () => {
		const params: any = {
			prop: 1,
		};

		http.get('/test', params);

		sinon.assert.calledOnce(mockHttpImplementation.get);
		const args: any = mockHttpImplementation.get.firstCall.args;
		expect(args[0]).to.equal('/test');
		expect(args[1].search.toString()).to.equal('prop=1');
	});

	it('should default to an empty string for null or undefined parameters', (): void => {
		const params: any = {
			nullParam: null,
			undefinedParam: undefined,
		};

		http.get('/test', params);

		sinon.assert.calledOnce(mockHttpImplementation.get);
		const args: any = mockHttpImplementation.get.firstCall.args;
		expect(args[0]).to.equal('/test');
		expect(args[1].search.toString()).to.equal('nullParam=&undefinedParam=');
	});

	it('should include false parameters as \'false\'', () => {
		const params: any = {
			falseParam: false,
		};

		http.get('/test', params);

		sinon.assert.calledOnce(mockHttpImplementation.get);
		const args: any = mockHttpImplementation.get.firstCall.args;
		expect(args[0]).to.equal('/test');
		expect(args[1].search.toString()).to.equal('falseParam=false');
	});

	it('should make a post request with the data jsonized and a json content header', () => {
		const data = { prop: 'string' };

		http.post('/test', data);

		sinon.assert.calledOnce(mockHttpImplementation.post);
		const args: any = mockHttpImplementation.post.firstCall.args;
		expect(args[0]).to.equal('/test');
		expect(args[1]).to.equal(JSON.stringify(data));
		const options: RequestOptions = args[2];
		expect(options.headers.has('Content-Type')).to.be.true;
		expect(options.headers.get('Content-Type')).to.equal('application/json');
	});

	it('should make a put request with the data jsonized and a json content header', () => {
		const data = { prop: 'string' };

		http.put('/test', data);

		sinon.assert.calledOnce(mockHttpImplementation.put);
		const args: any = mockHttpImplementation.put.firstCall.args;
		expect(args[0]).to.equal('/test');
		expect(args[1]).to.equal(JSON.stringify(data));
		const options: RequestOptions = args[2];
		expect(options.headers.has('Content-Type')).to.be.true;
		expect(options.headers.get('Content-Type')).to.equal('application/json');
	});

	it('should parse the response from json', (): void => {
		const putStream: Subject<any> = new Subject();
		const response: any = {
			_body: 'content',
			json: sinon.spy(),
		};
		mockHttpImplementation.put = sinon.spy(() => putStream);

		http.put('/test', {}).subscribe();
		putStream.next(response);

		sinon.assert.calledOnce(response.json);
	});

	it('should not try to parse if the response is empty', (): void => {
		const putStream: Subject<any> = new Subject();
		const response: any = {
			_body: '',
			json: sinon.spy(),
		};
		mockHttpImplementation.put = sinon.spy(() => putStream);

		http.put('/test', {}).subscribe();
		putStream.next(response);

		sinon.assert.notCalled(response.json);
	});

	it('should make a delete request with any specified query string parameters', () => {
		const params: any = {
			prop: 1,
		};

		http.delete('/test', params);

		sinon.assert.calledOnce(mockHttpImplementation.delete);
		const args: any = mockHttpImplementation.delete.firstCall.args;
		expect(args[0]).to.equal('/test');
		expect(args[1].search.toString()).to.equal('prop=1');
	});
});